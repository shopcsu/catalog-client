import Category from '~/plugins/api/models/Category'
import Login from '~/plugins/api/models/Login'
import SignUp from '~/plugins/api/models/SignUp'
import CartItems from '~/plugins/api/models/CartItems'
import Order from '~/plugins/api/models/Order'

export class ApiClass {
  constructor ({ client, store }) {
    this._client = client
    this.$store = store
  }

  get $client () {
    if (this.$store.getters.isAuthorized) {
      this._client.setToken(`Bearer ${this.$store.getters.authToken}`)
    }
    return this._client
  }

  async login (model) {
    return await this.$client.$post('/auth', new Login(model))
  }

  async signUp (model) {
    return await this.$client.$post('/auth/signup', new SignUp(model))
  }

  async readCategory (id) {
    return new Category(await this.$client.$get(id ? `/catalog/categories/${id}` : '/catalog/categories'))
  }

  async cartItems () {
    return new CartItems(await this.$client.$get('/cart'))
  }

  async addItemToCart (id, quantity) {
    return await this.$client.$post('/cart', { id, quantity })
  }

  async deleteItemFromCart (id) {
    return await this.$client.$delete(`/cart/${id}`)
  }

  async createOrder () {
    return await this.$client.$post('/cart/buy')
  }

  async orderList () {
    return (await this.$client.$get('/orders')).map(e => new Order(e))
  }

  async cancelOrder (id) {
    return (await this.$client.$patch(`/orders/cancel/${id}`))
  }
}
