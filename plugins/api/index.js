import { ApiClass } from '~/plugins/api/ApiClass'

export default (ctx, inject) => {
  const api = new ApiClass({
    client: ctx.$axios,
    store: ctx.store
  })

  ctx.$api = api
  inject('api', api)
}
