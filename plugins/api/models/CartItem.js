import { ActiveModel } from '@alt-point/active-models'

export default class CartItem extends ActiveModel {
  static get fillable () {
    return ['id', 'title', 'price', 'quantity']
  }

  static get $attributes () {
    return {
      id: 0,
      title: '',
      price: 0,
      quantity: 0
    }
  }
}
