import { ActiveModel } from '@alt-point/active-models'
import CartItem from '~/plugins/api/models/CartItem'

export default class CartItems extends ActiveModel {
  static get fillable () {
    return ['totalPrice', 'items']
  }

  static setterItems (model, prop, value, receiver) {
    Reflect.set(model, prop, value && value.length ? value.map(e => new CartItem(e)) : [], receiver)
  }

  static get $attributes () {
    return {
      totalPrice: 0,
      items: []
    }
  }
}
