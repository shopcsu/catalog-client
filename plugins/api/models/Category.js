import { ActiveModel } from '@alt-point/active-models'
import Item from '~/plugins/api/models/Item'
import CategoryShort from '~/plugins/api/models/CategoryShort'

export default class Category extends ActiveModel {
  static get fillable () {
    return [
      'id', 'title', 'items', 'children'
    ]
  }

  static setterItems (model, prop, value, receiver) {
    Reflect.set(model, prop, value && value.length ? value.map(e => new Item(e)) : [], receiver)
  }

  static setterChildren (model, prop, value, receiver) {
    Reflect.set(model, prop, value && value.length ? value.map(e => new CategoryShort(e)) : [], receiver)
  }

  static get $attributes () {
    return {
      id: 0,
      title: '',
      children: [],
      items: []
    }
  }
}
