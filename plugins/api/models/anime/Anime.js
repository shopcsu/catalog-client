import { ActiveModel, Enum } from '@alt-point/active-models'
import Theme from '~/plugins/api/models/anime/Theme'
import Genre from '~/plugins/api/models/anime/Genre'
import Studio from '~/plugins/api/models/anime/Studio'
import AnimeImages from '~/plugins/api/models/anime/AnimeImages'

const Types = new Enum(['TV', 'MOVIE', 'OVA', 'SPECIAL', 'ONA', 'MUSIC'], 'TV')
// const Statuses = new Enum(['AIRING', 'COMPLETE', 'UPCOMING'], 'AIRING')

export default class Anime extends ActiveModel {
  static get fillable () {
    return [
      'mal_id',
      'url',
      'images',
      'title',
      'title_english',
      'title_japanese',
      'type',
      'source',
      'episodes',
      'status',
      'duration',
      'rating',
      'score',
      'scored_by',
      'rank',
      'popularity',
      'members',
      'favorites',
      'synopsis',
      'background',
      'season',
      'year',
      'studios',
      'genres',
      'themes'
    ]
  }

  static setterImages (model, prop, value, receiver) {
    Reflect.set(model, prop, new AnimeImages(value), receiver)
  }

  static setterThemes (model, prop, value, receiver) {
    Reflect.set(model, prop, value && value.length ? value.map(e => new Theme(e)) : [], receiver)
  }

  static setterGenres (model, prop, value, receiver) {
    Reflect.set(model, prop, value && value.length ? value.map(e => new Genre(e)) : [], receiver)
  }

  static setterStudios (model, prop, value, receiver) {
    Reflect.set(model, prop, value && value.length ? value.map(e => new Studio(e)) : [], receiver)
  }

  static setterType (model, prop, value, receiver) {
    Reflect.set(model, prop, value && Types.values().includes(value) ? value : Types.default, receiver)
  }

  static get $attributes () {
    return {
      mal_id: 0,
      url: '',
      images: new AnimeImages(),
      title: '',
      title_english: '',
      title_japanese: '',
      type: Types.default,
      source: '',
      episodes: 0,
      status: '',
      duration: '',
      rating: 'G - All Ages',
      score: 0,
      scored_by: 0,
      rank: 0,
      popularity: 0,
      members: 0,
      favorites: 0,
      synopsis: '',
      background: '',
      season: 'Summer',
      year: 0,
      studios: [],
      genres: [],
      themes: []
    }
  }
}
