import { ActiveModel } from '@alt-point/active-models'
import Anime from '~/plugins/api/models/anime/Anime'

export default class AnimePagination extends ActiveModel {
  static get fillable () {
    return ['data', 'pagination']
  }

  static setterData (model, prop, value, receiver) {
    Reflect.set(model, prop, value && value.length ? value.map(e => new Anime(e)) : [], receiver)
  }

  static get $attributes () {
    return {
      data: [],
      pagination: {}
    }
  }
}
