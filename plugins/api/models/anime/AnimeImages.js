import { ActiveModel } from '@alt-point/active-models'
import AnimeImage from '~/plugins/api/models/anime/AnimeImage'

export default class AnimeImages extends ActiveModel {
  static get fillable () {
    return ['jpg', 'webp']
  }

  static setterJpg (model, prop, value, receiver) {
    Reflect.set(model, prop, new AnimeImage(value), receiver)
  }

  static setterWebp (model, prop, value, receiver) {
    Reflect.set(model, prop, new AnimeImage(value), receiver)
  }

  static get $attributes () {
    return {
      jpg: new AnimeImage(),
      webp: new AnimeImage()
    }
  }
}
