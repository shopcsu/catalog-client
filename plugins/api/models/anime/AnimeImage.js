import { ActiveModel } from '@alt-point/active-models'

export default class AnimeImage extends ActiveModel {
  static get fillable () {
    return ['image_url', 'small_image_url', 'large_image_url']
  }

  static get $attributes () {
    return {
      image_url: '',
      small_image_url: '',
      large_image_url: ''
    }
  }
}
