import { ActiveModel } from '@alt-point/active-models'

export default class Genre extends ActiveModel {
  static get fillable () {
    return ['mal_id', 'type', 'name', 'url']
  }

  static get $attributes () {
    return {
      mal_id: 0,
      type: '',
      name: '',
      url: ''
    }
  }
}
