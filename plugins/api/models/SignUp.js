import { ActiveModel } from '@alt-point/active-models'

export default class SignUp extends ActiveModel {
  static get fillable () {
    return ['name', 'password']
  }

  static get $attributes () {
    return {
      name: '',
      password: ''
    }
  }
}
