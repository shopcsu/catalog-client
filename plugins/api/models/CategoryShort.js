import { ActiveModel } from '@alt-point/active-models'

export default class CategoryShort extends ActiveModel {
  static get fillable () {
    return [
      'id', 'title'
    ]
  }

  static get $attributes () {
    return {
      id: 0,
      title: ''
    }
  }
}
