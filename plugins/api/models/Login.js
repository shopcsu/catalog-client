import { ActiveModel } from '@alt-point/active-models'

export default class Login extends ActiveModel {
  static get fillable () {
    return ['name', 'password']
  }

  static get $attributes () {
    return {
      name: '',
      password: ''
    }
  }
}
