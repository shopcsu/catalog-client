import { ActiveModel, Enum } from '@alt-point/active-models'
import CartItem from '~/plugins/api/models/CartItem'

const Statuses = new Enum(['confirmed', 'notConfirmed', 'canceled', 'finished'], 'notConfirmed')

// function validateStatus (status) {
//   const statuses = Statuses.values().map(e => e.toLowerCase())
//   return statuses.includes(status)
// }

export default class Order extends ActiveModel {
  static get fillable () {
    return ['id', 'status', 'items']
  }

  static setterItems (model, prop, value, receiver) {
    Reflect.set(model, prop, value && value.length ? value.map(e => new CartItem(e)) : [], receiver)
  }

  // static setterStatus (model, prop, value, receiver) {
  //   Reflect.set(model, prop, value && validateStatus(value) ? , receiver)
  // }

  static get $attributes () {
    return {
      id: '',
      status: Statuses.default,
      items: []
    }
  }
}
