export const formCreateEditMixin = (ModelConstructor, defaultData = {}) => ({
  model: {
    prop: 'value',
    event: 'save'
  },
  props: {
    id: {
      type: [String, Number],
      default () {
        return ''
      }
    },
    value: {
      type: ModelConstructor,
      default () {
        return new ModelConstructor(defaultData)
      }
    },
    serverMode: {
      type: Boolean,
      default () {
        return true
      }
    }
  },
  data () {
    return {
      FORM_NAME: `${ModelConstructor.name}Form`,
      model: new ModelConstructor(defaultData),
      loading: false
    }
  },
  watch: {
    id: {
      immediate: true,
      handler (n, p) {
        if (!this.serverMode) {
          return
        }
        if (n === p) {
          return
        }

        this.load()
      }
    },
    value: {
      immediate: true,
      handler (n, p) {
        if (this.serverMode) {
          return
        }
        if (
          JSON.stringify(n) !== JSON.stringify(p) ||
          JSON.stringify(n) !== JSON.stringify(this.model)
        ) {
          this.load()
        }
      }
    }
    // loading (n, p) {
    //   if (n === p) {
    //     return
    //   }
    //   if (n) {
    //     this.$nuxt.$loading.start()
    //   } else {
    //     this.$nuxt.$loading.finish()
    //   }
    // }
  },
  methods: {
    async load () {
      if (!this.id) {
        this.model = new ModelConstructor(this.value)
        return
      }
      this.loading = true
      this.model = new ModelConstructor(await this.loadModel(this.id))
      this.loading = false
      this.$emit('set-model', this.model)
    },
    async save (silent = false) {
      if (!this.serverMode) {
        this.saveModel()
        return
      }

      this.loading = true
      try {
        if (this.id) {
          const result = await this.update(this.id, this.model)
          this.$emit('update', result)
        } else {
          const result = await this.create(this.model)
          this.$emit('create', result)
        }
        if (!silent) {
          this.saveModel()
        }
        this.loading = false
      } catch (e) {
        console.error(e)
        this.loading = false
      }
    },
    saveModel () {
      this.$emit('save', new ModelConstructor(this.model))
    },
    loadModel (id) {},
    create (model) {},
    update (id, model) {}
  }
})
