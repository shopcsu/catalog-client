import Authorization from '~/plugins/api/models/Authorization'

export const state = () => ({
  auth: new Authorization()
})

export const mutations = {
  SET_TOKEN (s, { authToken, refreshToken, expiresAt }) {
    s.auth.authToken = authToken
    s.auth.refreshToken = refreshToken
    s.auth.expiresAt = expiresAt
  },
  LOGOUT (s) {
    s.auth = new Authorization()
  }
}

export const actions = {
  async login ({ commit }, model) {
    const response = await this.$api.login(model)
    commit('SET_TOKEN', response)
  },
  async signUp ({ commit }, model) {
    const response = await this.$api.signUp(model)
    commit('SET_TOKEN', response)
  },
  logout ({ commit }) {
    commit('LOGOUT')
  }
}

export const getters = {
  authToken: s => s.auth.authToken,
  refreshToken: s => s.auth.refreshToken,
  expiresAt: s => s.auth.expiresAt,
  isAuthorized: s => s.auth.authToken && s.auth.refreshToken && s.auth.expiresAt
}
